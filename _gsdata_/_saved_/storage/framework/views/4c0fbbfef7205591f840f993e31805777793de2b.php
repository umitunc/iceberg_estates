<?php echo strip_tags($header); ?>


<?php echo strip_tags($slot); ?>

<?php if(isset($subcopy)): ?>

<?php echo strip_tags($subcopy); ?>

<?php endif; ?>

<?php echo strip_tags($footer); ?>

<?php /**PATH /home/euro/otto2020.euro.kim/resources/views/vendor/mail/text/layout.blade.php ENDPATH**/ ?>