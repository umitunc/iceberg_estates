<?php 
//sınav bitiş apisi * daha önce uygulamaya girilmiş mi
$varmi = db("uygulamalar")
->where("model_id",post("model_id"))
->where("model_yetki_id",post("model_yetki_id"))
->where("type",post("type"))
->where("uid",u()->id)
->whereNull("datetime2")
->first();
$model = db("contents")->where("id",post("model_id"))->first();
$model_yetki = db("contents")->where("id",post("model_yetki_id"))->first();

if($varmi) { //eğer daha önce bir sınava giriş yapılmışsa 
    $score = ((strtotime("now") - strtotime($varmi->datetime1)) / 60)*$varmi->kat_sayi ; 
    db("uygulamalar")
    ->where("id",$varmi->id)
    ->update([
        "type" => post("type"),
        "model_id" => post("model_id"),
        "model_yetki_id" => post("model_yetki_id"),
        "json" => post("json"),
        "datetime2" => simdi(),
        "score" => $score,
        "sure" => (strtotime("now") - strtotime($varmi->datetime1)) / 60
    ]);
    echo "ok";
} else {
    echo "false";
}
?> 