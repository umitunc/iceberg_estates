<?php 
unset($_POST['email']);
unset($_POST['password']);
$postcodes = j(file_get_contents("https://api.postcodes.io/postcodes/".post("postalcode")));
if($postcodes['result']) {
    if($postcodes['result']['country']=="England") {
        $json = $_POST;
        $json['detail'] = $postcodes['result'];
        $id = ekle(
            [
                "json" => json_encode_tr($json),
                "date" => post("app_date")
            ]
            ,"appointments");
            $result = [
                'appointment_id'=> $id,
                "appointment_detail" => $postcodes['result'],
                "appointment_result" => "Appointment created is success"
            ];
            $varmi = db("contacts")->where("title",post("app_email"))
            ->where("uid",u()->id)
            ->first();
            if(!$varmi) {
                $contact_id = ekle([
                    "json" => json_encode_tr($_POST),
                    "title" => post("app_email"),
                    "date" => post("app_date")
                ],"contacts");
                $result['contact_result'] = "New contact created";
                $result['contact_id'] = $contact_id;
            } else {
                $result['contact_result'] = "Contact already created";
                $result['contact_id'] = $varmi->id;
            }
            
            $result = json_encode_tr($result);
        echo $result;
    } else {
        endpoint(['error'=>"The postal code you entered could not be added because it is outside the UK borders."]);
    }
} else {
    endpoint(['error'=>"Postcode not found. Please make sure you have entered a correct postal code"]);
}



?>