<?php $ogrenciler = usersArray(); ?>

<div class="content">
   <div class="row">
       {{col("col-md-12","Uygulaması Devam Eden Öğrenciler",6)}}
        <?php $sorgu  = db("uygulamalar")->whereNull("datetime2")->orderBy("datetime1","ASC")->get() ?>
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <th>{{e2("Öğrenci Adı")}}</th>
                    <th>{{e2("Bölümü")}}</th>
                    <th>{{e2("Uygulama Türü")}}</th>
                    <th>{{e2("Başlangıç")}}</th>
                    <th>{{e2("Süre")}}</th>
                    <th>{{e2("Bilgi")}}</th>
                </tr>
                <?php foreach($sorgu AS $s)  { 
                    if(isset($ogrenciler[$s->uid])) { 
                     $j = j($s->json);
                     $ogrenci = $ogrenciler[$s->uid];
                   ?>
                  <tr>
                      <td>{{$ogrenci->name}} {{$ogrenci->surname}}</td>
                     
                      <td>{{$ogrenci->branslar}}</td>
                     
                      <td>{{$s->type}}</td>
                      <td>{{date("d.m.Y H:i",strtotime($s->datetime1))}}</td>
                        <td></td>
                      <td>{{print2($j)}}</td>
                  </tr>  
                     <?php } ?>
                 <?php } ?>
            </table>
        </div>
       {{_col()}}
       {{col("col-md-12","Uygulamayı Tamamlanan Öğrenciler",4)}}
       <?php $sorgu  = db("uygulamalar")->whereNotNull("datetime2")->orderBy("datetime2","DESC")->get() ?>
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <th>{{e2("Öğrenci Adı")}}</th>
                    <th>{{e2("Bölümü")}}</th>
                    <th>{{e2("Uygulama Türü")}}</th>
                    <th>{{e2("Başlangıç")}}</th>
                    <th>{{e2("Bitiş")}}</th>
                    <th>{{e2("Süre")}}</th>
                    <th>{{e2("Sonuç")}}</th>
                    <th>{{e2("Bilgi")}}</th>
                </tr>
                <?php foreach($sorgu AS $s)  { 
                    if(isset($ogrenciler[$s->uid])) { 
                     $j = j($s->json);
                     $ogrenci = $ogrenciler[$s->uid];
                   ?>
                  <tr>
                      <td>{{$ogrenci->name}} {{$ogrenci->surname}}</td>
                     
                      <td>{{$ogrenci->branslar}}</td>
                     
                      <td>{{$s->type}}</td>
                      <td>{{date("d.m.Y H:i",strtotime($s->datetime1))}}</td>
                      <td>{{date("d.m.Y H:i",strtotime($s->datetime2))}}</td>
                      <td>{{$s->sure}}</td>
                      <td>{{$s->score}}</td>
                      <td>{{print2($j)}}</td>
                  </tr>  
                     <?php } ?>
                 <?php } ?>
            </table>
        </div>
       {{_col()}}
   </div>
</div>