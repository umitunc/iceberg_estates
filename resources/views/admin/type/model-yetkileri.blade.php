<?php 
if(getisset("guncelle")) {
   //
    $title = "";
	$title2 = "";
	unset($_POST['_token']);
	$_POST['datetime1'] = date("Y-m-d H:i:s",strtotime($_POST['datetime1']));
	if(postisset("title")) {
		$_POST['title'] = implode(",",$_POST['title']);
	}
	if(postisset("title2")) {
		$_POST['title2'] = implode(",",$_POST['title2']);
	}
    db("contents")
    ->where("id",get("guncelle"))
    ->update($_POST);
	bilgi("Bilgiler başarıyla güncellendi");
//	print2($_POST);
	exit();
} 
if(getisset("yetki-guncelle")) {
    print2($_POST);
    $title = implode(",",$_POST['title']);
    echo $title;
    db("contents")
    ->where("id",get("yetki-guncelle"))
    ->update([
        "title" => $title
    ]);
	exit();
} 
if(getisset("yetki-guncelle2")) {
    print2($_POST);
    $title = implode(",",$_POST['title2']);
    echo $title;
    db("contents")
    ->where("id",get("yetki-guncelle2"))
    ->update([
        "title2" => $title
    ]);
	exit();
} 

?>
<div class="content">
	<div class="block">
		<div class="block-header block-header-default">
			<h3 class="block-title"><i class="fa fa-{{$c->icon}}"></i> {{e2("Model Yetkileri")}}</h3>
			<div class="block-options">
				<div class="block-options-item"> <a
						href="{{ url('admin-ajax/content-type-blank-delete?type='. $c->title) }}"
						teyit="{{__('Tüm boş '.$c->title.'  '._('') )}}" title="{{_('Boş Olan  İçeriklerini Sil')}}"
						class="btn btn-danger"><i class="fa fa-times"></i> </a> <a
						href="{{ url('admin-ajax/content-type-add?type='. $c->title) }}" class="btn btn-success"
						title="Yeni {{$c->title}} {{_('İçeriği Oluştur')}}"><i class="fa fa-plus"></i> </a> </div>
			</div>
		</div>
		<div class="block-content">
			<div class="js-gallery "> @include("admin.inc.table-search") <div class="table-responsive">
					<div class="result-ajax"></div>
					<table class="table table-striped table-hover table-bordered table-vcenter">
						<thead>
							<tr>
                                <th>{{__("Uygulama Türü")}}</th>
								<th>{{__("Model")}}</th>
								
								<th>{{__("Yetkilenecek Sınıflar")}}</th>
								<th>{{__("Yetkilenecek Öğrenciler")}}</th>
                                <th>{{e2("Başlangıç Tarihi")}}</th>
                                <th>{{e2("Bitiş Tarihi")}}</th>
                                <th>{{e2("Süre")}}</th>
                                <th>{{e2("Dakika Puan Katsayısı")}}</th>
								<th class="text-center" style="width: 100px;">{{__("İşlemler")}}</th>
							</tr>
						</thead>
						<?php 
							$modeller = contents("Modeller"); 
							$siniflar = contents("Sınıflar"); 
							$ogrenciler = contents("Öğrenciler");
                            $types = explode(",","Sınav,Eğitim");
						?>
						<tbody> @foreach($alt AS $a) 
						<form action="?guncelle={{$a->id}}" method="post" class="serialize">
							<tr class="">
                                <td>
                                    <select name="alt_type" required id="" table="contents"  class="form-control select2 iduzenle">
                                            <option value="">{{e2("Uygulama Türünü Seçiniz")}}</option>
                                        <?php  
                                       
                                        foreach($types AS $t) { ?>
                                            <option value="{{$t}}" <?php if($t==$a->alt_type) echo "selected"; ?>>{{$t}}</option>
                                            <?php } ?>

                                    </select>
                                </td>
								<td><?php // print2($a); ?>
                                    <select name="kid" id="" table="contents"  class="form-control select2 iduzenle">
                                            <option value="">{{e2("Model Seçiniz")}}</option>
                                        <?php  foreach($modeller AS $m) { ?>
                                            <option value="{{$m->title}}" <?php if($m->title==$a->kid) echo "selected"; ?>>{{$m->title}}</option>
                                            <?php } ?>

                                    </select>
								 </td>
								<td>
									<?php $yetkilenen = explode(",",$a->title); ?>
                                   
                                        {{csrf_field()}}
                                        <select name="title[]" id=""  class="form-control select2 " multiple>
                                            
                                            <?php foreach($siniflar AS $m) { ?>
                                                <option value="{{$m->title}}" <?php if(in_array($m->title,$yetkilenen)) echo "selected"; ?>>{{$m->title}}</option>
                                                <?php } ?>

                                        </select>
                                  
								 </td>
								<td>
									<?php $yetkilenen = explode(",",$a->title2); ?>
                                 
                                        {{csrf_field()}}
                                        <select name="title2[]" id=""  class="form-control select2 " multiple>
                                            
                                            <?php foreach($ogrenciler AS $m) { ?>
                                                <option value="{{$m->title}}" <?php if(in_array($m->title,$yetkilenen)) echo "selected"; ?>>{{$m->title}}</option>
											<?php } ?>

                                        </select>
                               

								 </td>
								<td>
									<input type="datetime-local" name="datetime1" value="{{date("Y-m-d\TH:i:s", strtotime($a->datetime1))}}" class="form-control" id="">
								</td>
								<td>
									<input type="datetime-local" name="datetime2" value="{{date("Y-m-d\TH:i:s", strtotime($a->datetime2))}}" class="form-control" id="">
								</td>
                                <td>
									<input type="number" step="any" name="sure" value="{{$a->sure}}" class="form-control" id="">
								</td>
                                <td>
									<input type="number" step="any" name="kat_sayi" value="{{$a->kat_sayi}}" class="form-control" id="">
								</td>
								<td>
									<div class="btn-group btn-block"> <a href="{{ url('admin/contents/'. $a->slug) }}"
											class="btn btn-secondary js-tooltip-enabled" data-toggle="tooltip" title=""
											data-original-title="Edit"> <i class="fa fa-edit"></i> </a> <a
											href="{{ url('admin/contents/'. $a->slug .'/delete') }}"
											teyit="{{$a->title}} {{__('içeriğini silmek istediğinizden emin misiniz?')}}"
											title="{{$a->title}} {{__('Silinecek!')}}"
											class=" btn  btn-secondary js-tooltip-enabled" data-toggle="tooltip"
											title="" data-original-title="Delete"> <i class="fa fa-times"></i> </a>
									</div>
									<button class="btn btn-primary" type="submit"><i class="fa fa-sync"></i> {{e2("Bilgileri Güncelle")}}</button>

								</td>
							</tr> 
							</form>

							@endforeach </tbody>
					</table> {{$alt->fragment('alt')->links() }}
				</div>
			</div>
		</div>
	</div>
</div>