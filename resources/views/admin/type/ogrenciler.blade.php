@extends('admin.master')
@section("title",__("Kullanıcılar"))
@section("desc",__("Sistemde yer alan kullanıcıları bu bölümden yönetebilirsiniz"))
@section('content')
<?php
	use Illuminate\Support\Facades\Request;
	use Illuminate\Support\Facades\Input;
	use App\Contents;
	use App\Types;
	use App\Fields;
	use App\User;
	try{
		$seviye = Contents::where("slug","user-level")->first();
		$seviye = strip_tags($seviye->html);
		$seviye = explode(",",$seviye);
	} catch (Exception $e) {
		
	}
	$seviye = "Admin
	Kurum
	Akademisyen
	Öğrenci";
	$seviye = explode("\n",$seviye);
	$request = null;
	if(Input::has("q")) {
		  $request = Request::all();
		  $q = $request['q'];
			
		  $searchFields = ['name','surname','email','phone','permissions'];
		  $users = User::where(function($query) use($request, $searchFields){
			$searchWildcard = '%' . $request['q'] . '%';
			foreach($searchFields as $field){
			  $query->orWhere($field, 'LIKE', $searchWildcard);
			}
		  });
		  $users = $users->where("id",">=",Auth::user()->id);
          $users = $users->where("level","Öğrenci");
		  $users = $users->simplePaginate(10);

	} else {
		$users = User::orderBy("id","DESC")->where("id",">=",Auth::user()->id);
		$users = $users->where("level","Öğrenci");
		$users = $users->simplePaginate(5);
	}
	
	$types = Types::all();
	
?>
<div class="content">
<div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">
			
				<form action="" method="get">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<div class="input-group-prepend">
									<button type="submit" class="btn btn-secondary">
										<i class="fa fa-search"></i>
									</button>
								</div>
								<input type="text" class="form-control"  name="q" value="{{@$request['q']}}" placeholder="{{e2("Kullanıcı Adı")}}">
							</div>
						</div>
					</div>
				</form>
			
			</h3>
            <div class="block-options">
                <div class="block-options-item">
                    <a href="{{ url('admin-ajax/user-add?type=Öğrenci') }}" class="btn btn-default"><i class="fa fa-plus"></i></a>
                </div>
            </div>
        </div>
		

        <div class="block-content">
			<div class="table-responsive">
				<table class="table table-bordered table-hover table-striped">
					<tr>
						<td>{{e2("Kimlik")}}</td>
						<td>{{e2("Adı")}}</td>
						<td>{{e2("Soyadı")}}</td>					
						<td>{{e2("E-Mail")}}</td>
						<td>{{e2("Telefon")}}</td>
						<td>{{e2("Sınıf")}}</td>
						<td>{{e2("Şifre")}}</td>
						<td>{{e2("Kurtarma Şifresi")}}</td>
						<td>{{e2("Etki Alanı")}}</td>
						<td>{{e2("İşlem")}}</td>
					</tr>
					@foreach($users AS $u)
						
					<tr>
						<td>{{$u->id}}</td>
						<td><input type="text" name="name" value="{{$u->name}}" table="users" id="{{$u->id}}" class="name{{$u->id}} form-control edit" /></td>
						<td><input type="text" name="surname" value="{{$u->surname}}" table="users" id="{{$u->id}}" class="surname{{$u->id}} form-control edit" /></td>
						
						<td><input type="text" name="email" value="{{$u->email}}" table="users" id="{{$u->id}}" class="email{{$u->id}} form-control edit" /></td>
						<td><input type="text" name="phone" value="{{$u->phone}}" table="users" id="{{$u->id}}" class="phone{{$u->id}} form-control edit" /></td>
						
						<td>
                            <?php $sinif = contents("Sınıflar");  ?>
                            <select name="branslar" id="{{$u->id}}" class="form-control edit" table="users">
                                <option value="">{{e2("Seçiniz")}}</option>
                                <?php foreach($sinif AS $s) { ?>
                                    <option value="{{$s->title}}" <?php if($u->branslar==$s->title) echo "selected"; ?>>{{$s->title}}</option>
                                <?php } ?>
                            </select>    
                        </td>
                        <td><a href="{{url('admin-ajax/password-update?id='.$u->id)}}" title="{{__('Kullanıcının şifresini sıfırla')}}" class="btn btn-default"><i class="fa fa-sync"></i> {{e2("Şifre Sıfırla")}}</button></td>
						<td>{{$u->recover}}</td>
						<td><input type="text" name="alias" value="{{$u->alias}}" table="users" id="{{$u->id}}" class="alias{{$u->id}} form-control edit" /></td>

						<td>
						<div class="dropdown">
						  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							{{e2("İşlemler")}}
						  </button>
						  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="#"><i class="fa fa-lock"></i> {{e2("Giriş Yap")}}</a>
							<a class="dropdown-item" teyit="{{$u->adi}} {$u->soyadi} {{e2("Öğrencisini silmek istediğinizden emin misiniz?")}}" href="{{url('admin-ajax/user-delete?id='.$u->id)}}">
							<i class="fa fa-times"></i>
							{{e2("Sil")}}</a>
						  </div>
						</div>
						</td>
					</tr>
					@endforeach
				</table>
				{{$users->fragment('users')->links() }}
			</div>
		</div>
@endsection
