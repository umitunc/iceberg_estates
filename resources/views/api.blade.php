

<?php 

$except = explode(",","register"); //permissions except list
$devam = true;
if(!in_array($route,$except)) {
    if(postisset("token")) {
        $user = db("users")
        ->where("token",post("token"))
        ->where("token_exp",">",simdi())
        ->first();
        if($user) {
            Auth::loginUsingId($user->id);
        } else {
           // echo json_encode_tr(['error' => "Token not found or token expired"]);
            endpoint(['error' => "Token not found or token expired"]);
            $devam = false;
        }
    } else {
        $email = post("email");
        $password = post("password");
        if (Auth::attempt(array('email' => $email, 'password' => $password))){
            $token = Str::random(40);
            db("users")
            ->where("id",u()->id)
            ->update([
                "token_exp" => date("Y-m-d H:i:s",strtotime("+30 days")),
                "token" => $token
            ]);
          
        } else {
            echo "0\n";
            exit();
        }
    }
    
}

//print2($_GET);
//echo $route;
if($devam) { 
 
    if(isset($route)) {
        ?>
        @if(View::exists("api.$route"))
            @include("api.$route")
        @else 
            Geçerli bir yönlendirici bulunamadı
        @endif
        <?php 
    } 
 } 

 